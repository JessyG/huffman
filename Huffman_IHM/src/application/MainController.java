package application;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import huffman.Huffman;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class MainController implements Initializable {
	// TextField pour afficher les fichiers s�lectionn�s
	@FXML
	private TextField textSource;
	// TextField pour afficher le chemin absolu du r�pertoire s�lectionn�
	@FXML
	private TextField textDest;
	// ListView pour afficher la liste de tous les chemins absolus des fichiers s�lectionn�s
	@FXML
	private ListView<String> listFiles;
	
	private FileChooser fileChooser = new FileChooser();
	private DirectoryChooser directoryDest = new DirectoryChooser();
	private Huffman huf;
	// Liste des fichiers s�lectionn�s
	private List<File> files;
	private Alert dialogE;
	private Alert dialogConfirmation;
	private Alert dialogInformation;
	
	/**
	 * M�thode d'initialisation du programme
	 * On cr�e une nouvelle instant de la classe Huffman
	 * On pr�pare l'affichage des diff�rentes boites de dialogue
	 * On initialiste la liste des fichiers s�lectionn�s
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.huf = new Huffman();
		this.files = new ArrayList<File>();
		this.dialogE = new Alert(AlertType.ERROR);
		this.dialogConfirmation = new Alert(AlertType.CONFIRMATION);
		this.dialogInformation = new Alert(AlertType.INFORMATION);
		dialogE.setTitle("Erreur");
		this.dialogConfirmation.setTitle("Confirmation");
		this.dialogConfirmation.setHeaderText(null);
		this.dialogConfirmation.setContentText("Ce fichier existe d�j�, voulez-vous le remplacer ? ");
		this.dialogConfirmation.getButtonTypes().setAll(new ButtonType("Oui"), new ButtonType("Non"));
	}
	
	/**
	 * M�thode appel�e lorsque l'on appuie sur le bouton
	 * permettant de choisir une liste de fichiers
	 */
	@FXML
	public void get_list_files() {
		// on commence par r�initialiser les donn�es
		// afin d'avoir une nouvelle liste de fichiers
		// � chaque clique du bouton parcourir
		this.listFiles.getItems().clear();
		this.files.clear();
		this.textSource.setText("");
		List<File> files = new ArrayList<File>();
		// on r�cup�re la liste des fichiers s�lectionn�s
		// on ajoute chaque fichier � notre liste
		// et on affiche leur nom dans la view
		files = this.fileChooser.showOpenMultipleDialog(null);
		if(files != null) {
			for(File f : files) {
				this.listFiles.getItems().add(f.getAbsolutePath());
				this.textSource.setText(f.getName() + " ; " + this.textSource.getText());
				this.files.add(f);
			}
		}
	}
	
	/**
	 * M�thode appel�e lorsque l'on appuie sur le bouton
	 * permettant de choisir un r�pertoire de destination
	 */
	@FXML
	public void get_directory_dest() {
		File dir = this.directoryDest.showDialog(null);
		this.textDest.setText(dir.getAbsolutePath());
	}
	
	/**
	 * Ouvre une fen�tre de dialogue qui permet �
	 * l'utilisateur de choisir le nom du fichier compress�
	 * 
	 * @return
	 * 		Le nom choisi par l'utilisateur
	 */
	public String dialog() {
		TextInputDialog inDialog = new TextInputDialog("compress.huf");
		inDialog.setTitle("Name");
		inDialog.setHeaderText("Nom du fichier compress�");
		inDialog.setContentText("Nom :");
		Optional<String> textIn = inDialog.showAndWait();
		if(textIn.isPresent()) { 
			return textIn.get();
		}
		return null;
	}
	
	/**
	 * Ouvre une boite de dialogue d'erreur
	 * 
	 * @see MainController#compress()
	 * @see MainController#decompress()
	 */
	public void dialog_error(String header, String content) {
		dialogE.setHeaderText(header);
		dialogE.setContentText(content);
		dialogE.showAndWait();
	}
	
	/**
	 * Permet de s'assurer que des fichiers ainsi qu'un repertoire
	 * de destination ont �t� s�lectionn�s avant de cliquer sur les
	 * boutons "Compresser" ou "D�compresser"
	 * 
	 * @return
	 * 		0 : tout est bien rempli
	 * 		-1 : aucun fichier s�lectionn�
	 * 		-2 : aucun dossier de destination s�lectionn�
	 * 
	 * @see MainController#compress()
	 * @see MainController#decompress()
	 */
	public int check_data() {
		if(this.listFiles.getItems().isEmpty()) {
			dialog_error("Aucun fichi� trouv�", "Erreur : vous devez s�lectionner des fichiers");
			return -1;
		}
		if(this.textDest.getText().equals("")) {
			dialog_error("Dossier de destination", "Erreur : vous devez s�lectionner un dossier de destination");
			return -2;
		}
		return 0;
	}
	
	/**
	 * M�thode appel�e lorsque l'on clique sur le bouton "Compresser"
	 * La boite de dialogue affich�e d�pend du r�sultat de la compression
	 * 
	 * @see Huffman#compress(List, File)
	 */
	@FXML
	public void compress() {
		String name = "";
		File fileCompress;
		// on v�rifie que tous les champs ont bien �t� remplis
		if(check_data() != 0) return;
		if((name = dialog()) == null) return;
		fileCompress = new File(this.textDest.getText() + "\\" + name);
		// si le fichier compress� existe d�j�
		// on demande � l'utilisateur s'il souhaite le remplacer
		if(fileCompress.exists())
			if(this.dialogConfirmation.showAndWait().get().getText() == "Non")
				return;
		int result = this.huf.compress(this.files, fileCompress);
		if(result == -1) {
			dialog_error("cr�ation du fichier", "Erreur : cr�ation du fichier impossible");
			return;
		}
		if(result == -2) {
			dialog_error("Compression", "Erreur : probl�me de compression");
			return;
		}
		// la compression s'est termin�e
		// on envoie � l'utilisateur un message de confirmation
		this.dialogInformation.setTitle("Compression");
		this.dialogInformation.setContentText("Compression termin�e");
		this.dialogInformation.showAndWait();
	}
	
	/**
	 * M�thode appel�e lorsque l'on clique sur le bouton "D�compresser"
	 * La boite de dialogue affich�e d�pend du r�sultat de la d�compression
	 * 
	 * @see Huffman#decompress(List, String)
	 */
	@FXML
	public void decompress() {
		// on v�rifie que tous les champs ont �t� saisi
		if(check_data() != 0) return;
		int result = this.huf.decompress(this.files, this.textDest.getText());
		if(result == -1) {
			dialog_error("D�compression", "Erreur : format du fichier compress� incorrect");
			return;
		}
		if(result == -2) {
			dialog_error("D�compression", "Erreur : probl�me de d�compression");
			return;
		}
		// la d�compression est termin�e
		// on envoie � l'utilisateur un message de confirmation
		this.dialogInformation.setTitle("D�compression");
		this.dialogInformation.setContentText("D�compression termin�e");
		this.dialogInformation.showAndWait();
	}
}
