package huffman;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Classe principale permettant la compression et la décompression de
 * la donnée passée en paramètre des méthodes
 * 
 * @author Jessy
 *
 */
public class Huffman {
	/**
	 * Représente un séparateur utilisé dans lors de la création du fichier compressé
	 * afin de séparer les différente valeur de la table d'occurence
	 * 
	 * @see Huffman#insert_data(File, File, List<Integer>, String)
	 * @see Huffman#get_tab_occurence(InputBitStream)
	 */
	final String SEPARATOR = "//";
	
	/**
	 * Est rempli de manière à associer chaque caractère du fichier à compresser 
	 * à son code binaire correspondant
	 * 
	 * @see Huffman#create_code(Node)
	 * @see Huffman#traverse_all(Node, String)
	 * @see Huffman#compress_file(File, File, String)
	 */
	private HashMap<Character, String> codes;
	
	/**
	 * Tableau d'entiers de 256 cases afin d'y représenter
	 * toutes les combinaisons d'octets possible et de construire
	 * un tableau d'occurence après lecture du fichier à compresser
	 * 
	 * @see Huffman#Huffman()
	 * @see Huffman#create_tree(List)
	 * @see Huffman#read(File)
	 */
	private Integer[] occurence;
	
	/**
	 * Utilisé en cas de compression d'un dossier,
	 * la liste représente alors l'ensemble des fichiers
	 * du répertoire sélectionné
	 * 
	 * @see Huffman#compress_directory(File, File)
	 * @see Huffman#compress_all_directory(File, String)
	 */
	private List<String> filePathName;
	
	/**
	 * Utilisé en cas de compression d'un dossier,
	 * la liste représente alors l'ensemble des dossiers
	 * du répertoire sélectionné
	 * 
	 * @see Huffman#compress_directory(File, File)
	 * @see Huffman#compress_all_directory(File, String)
	 */
	private List<String> directoryPathName;
	
	/**
	 * Constructeur de Huffman
	 * 
	 * On s'en sert pour initialiser les différents attributs
	 * de la classe
	 */
	public Huffman() {
		this.codes = new HashMap<Character, String>();
		this.occurence = new Integer[256];
		this.filePathName = new ArrayList<String>();
		this.directoryPathName = new ArrayList<String>();
	}
	
	/**
	 * Créer le tableau d'occurence
	 * 
	 * @param f
	 * 		Le fichier dont on calcule les occurences
	 * 
	 * @return une List<Integer> pour facilité la récupération du tableau
	 * dans le fichier compressé
	 * 
	 * @throws IOException si jamais le fichier n'est pas trouvé
	 */
	public List<Integer> read(File f) throws IOException  {
		BufferedInputStream br = new BufferedInputStream(new FileInputStream(f));
		// on réinitialise la table à chaque appel
		for (int i = 0; i < 256; i++)
			occurence[i] = 0;
		for(int i = br.read(); i != -1; i = br.read())
			occurence[i]++;
		br.close();
		return Arrays.asList(this.occurence);
	}
	
	/**
	 * Crée l'arbre binaire correspondant au tableau d'occurence
	 * 
	 * @param occurences
	 * 		Le tableau d'occurence de la donnée sélectionnée
	 * 
	 * @return le noeud racine de l'arbre
	 */
	public Node create_tree(List<Integer> occurences) {
		// liste représentant toutes les feuilles de l'arbre
		ArrayList<Node> nodes = new ArrayList<Node>();
		for(int i = 0; i < occurences.size(); i++)
			if(occurences.get(i) > 0)
				nodes.add(new Node(occurences.get(i), (char) i));
		// si la liste de noeuds est vide
		// alors le fichier est vide, on retourne donc null
		if(nodes.size() == 0) return null;
		// tant qu'il y a plus d'un élément dans la liste
		// on fusionne les deux noeuds associés aux occurences
		// les plus faibles
		while(nodes.size() > 1)
			merge(nodes, extract_min(nodes), extract_min(nodes));
		// on retourne la racine de l'arbre
		return nodes.get(0);
	}
	
	/**
	 * Récupère le noeud le plus petit et l'extrait de la liste
	 * 
	 * @param n la liste des noeuds de l'arbre
	 * 
	 * @return le noeud le plus petit
	 */
	public Node extract_min(ArrayList<Node> n) {
		Node nMin =  Collections.min(n);
		n.remove(nMin);
		return nMin;
	}
	
	/**
	 * Fusionne 2 noeuds en sommant ses occurences
	 * les 2 noeuds sont retirés de la liste
	 * tandis qu'on y ajoute le noeud fusionné
	 * 
	 * @param nodes Liste des noeuds restant de l'arbre
	 * 
	 * @param n1 Noeud à fusionner et retirer de la liste
	 * 
	 * @param n2 Noeud à fusionner et retirer de la liste
	 */
	public void merge(ArrayList<Node> nodes, Node n1, Node n2) {
		Node nNew = new Node(n1.get_occurence() + n2.get_occurence(), null);
		nNew.set_left(n1);
		nNew.set_right(n2);
		nodes.add(nNew);
	}
	
	
	public void create_code(Node root) {
		this.codes.clear();
		if(root == null) return;
		traverse_all(root, "");
	}
	
	/**
	 * Récupère le code binaire associé à chaque symbole
	 * Pour cela, on associe le 0 pour le fils gauche
	 * et le 1 pour le fils droit
	 * 
	 * @param root
	 * 		Noeud racine de l'arbre de compression
	 * @param code
	 * 		code binaire de la feuille courante
	 * 
	 * @see Huffman#codes
	 */
	private void traverse_all(Node root, String code) {
		if (root != null) {
			// si le noeud est une feuille
			if(root.get_left() == null) {
				// on récupère le code obtenu et on l'ajoute
				// à notre HashMap
				this.codes.put(root.get_symbol(), code);
				code = "";
			}
			traverse_all(root.get_left(), code + "0");
			traverse_all(root.get_right(), code + "1");
	    }
	}
	
	/**
	 * On r�cup�re le code pour pouvoir tester la m�thode
	 * traverse_all dans la classe de test
	 */
	public HashMap<Character, String> get_code() {
		return this.codes;
	}
	
	/**
	 * Compresse le fichier ou le dossier sélectionné
	 * 
	 * @param fileToCompress
	 * 		La donnée à compresser
	 * 
	 * @param fileCompressName
	 * 		Le nom du fichier compressé
	 * 
	 * @return
	 * 		0 : compression effectu�e avec succ�s
	 * 		-1 : cr�ation du fichier compress� impossible
	 * 		-2 : probl�me de lecture dans le fichier � compresser ou d'�criture dans le fichier compress�
	 */
	public int compress(File fileToCompress, File fileCompress) {
		try {
			fileCompress.createNewFile();
		} catch(IOException e) {
			return -1;
		}
		try {
			OutputStreamWriter writeDataFileCompress = new OutputStreamWriter(new FileOutputStream(fileCompress, true), "Cp1252");
			// si la donnée à compresser est un répertoire
			// on appelle la méthode compress_directory
			// et on écrit le caractère "d" (pour dossier) au début
			// du fichier afin de facilité la décompression
			if(fileToCompress.isDirectory()) {
				writeDataFileCompress.write("d\n");
				writeDataFileCompress.close();
				compress_directory(fileToCompress, fileCompress);
			}
			// sinon, si c'est un dossier
			// on appelle la méthode compress_file
			// et on écrit le caractère "f" pour fichier
			// au début du fichier
			else {
				writeDataFileCompress.write("f\n");
				writeDataFileCompress.close();
				compress_file(fileToCompress, fileCompress, fileToCompress.getName());
			}
		} catch(IOException e) {
			return -2;
		}
		return 0;
	}
	
	/**
	 * Surcharge de la m�thode compress(File, File) pour permettre de compresser
	 * une liste de fichiers
	 * 
	 * @param fileToCompress
	 * 		La liste de fichier � compresser
	 * @param fileCompress
	 * 		Le fichier compress�
	 * 
	 * @return
	 * 		0 : compression effectu�e avec succ�s
	 * 		-1 : cr�ation du fichier compress� impossible
	 * 		-2 : probl�me de lecture dans le fichier � compresser ou d'�criture dans le fichier compress�
	 */
	public int compress(List<File> fileToCompress, File fileCompress) {
		try {
			fileCompress.createNewFile();
		} catch(IOException e) {
			return -1;
		}
		try {
			OutputStreamWriter writeDataFileCompress = new OutputStreamWriter(new FileOutputStream(fileCompress, true), "Cp1252");
			writeDataFileCompress.write("lf\n");
			writeDataFileCompress.write(fileToCompress.size() + "\n");
			writeDataFileCompress.close();
			for(File f : fileToCompress)
				compress_file(f, fileCompress, f.getName());
		} catch(IOException e) {
			return -2;
		}
		return 0;
	}
	
	/**
	 * Permet de compresser un dossier.
	 * Tous les répertoires et fichiers du dossier sélectionné sont compressés dans un unique fichier
	 * formaté de la façon suivante :
	 * 	1 -> première ligne : le caractère "d" pour reconnaitre le format d'un dossier en cas de décompression
	 * 	2 -> deuxième ligne : nombre de dossiers dans le répertoire sélectionné
	 *  3 -> lignes suivantes : nom de tous les dossiers (un par ligne)
	 *  4 -> ligne suivante : nombre de fichiers dans le répertoire sélectionné
	 *  5 -> ligne suivante : nom du premier fichier (son chemin à partir du dossier sélectionné)
	 *  6 -> ligne suivante : tableau d'occurence du fichier (pour récupérer l'arbre)
	 *  7 -> ligne suivante : nombre d'octet à lire
	 *  8 -> lignes suivantes : les données compressées
	 *  9 -> retour à l'étape 5 et ainsi de suite jusqu'à avoir lu tous les fichiers à compresser
	 *  
	 * @param directory
	 * 		Dossier à compresser
	 * 
	 * @param fileCompress
	 * 		Fichier représentant le résultat compressé du premier paramètre
	 * 
	 * @throws IOException en cas de problème de lecture et/ou d'écriture
	 */
	private void compress_directory(File directory, File fileCompress) throws IOException {
		String path = "";
		// on récupère la liste des fichiers et la liste
		// des dossiers présents dans directory
		this.filePathName.clear();
		this.directoryPathName.clear();
		get_all_files(directory, directory.getName());
		File currentFile;
		// on écrit dans le fichier compressé le nombre de
		// dossiers présents dans directory ainsi que le nom de chacun
		// d'entre-eux
		// enfin on écrit le nombre de fichiers présents dans directory
		OutputStreamWriter writeDataFileCompress = new OutputStreamWriter(new FileOutputStream(fileCompress, true));
		writeDataFileCompress.write(this.directoryPathName.size() + 1 + "\n");
		writeDataFileCompress.write(directory.getName() + "\n");
		if(this.directoryPathName.size() > 0) {
			for(String pathName : this.directoryPathName)
				writeDataFileCompress.write(pathName + "\n");
		}
		writeDataFileCompress.write(this.filePathName.size() + "\n");
		writeDataFileCompress.close();
		for(int i = 0; i < this.filePathName.size(); i++) {
			path = this.filePathName.get(i);
			currentFile = new File(path);
			compress_file(currentFile, fileCompress, path);
		}
	}
	
	/**
	 * Rempli filePathname et directoryPathName avant de lister le nom des fichiers
	 * et des dossiers présents dans directory
	 * 
	 * @param directory
	 * 		Le dossier que l'on souhaite compresser, on parcourra récursivement tous
	 * 		ses enfants
	 * 
	 * @param currentPathDirectory
	 * 		Chemin relatif au dossier courant
	 * 
	 * @see Huffman#filePathName
	 * @see Huffman#directoryPathName
	 */
	public void get_all_files(File directory, String currentPathDirectory) {
        for(File f : directory.listFiles()) {
            if(f.isDirectory()) {
            	this.directoryPathName.add(currentPathDirectory + "/" + f.getName());
            	get_all_files(f, currentPathDirectory + "/" + f.getName());
            }
            else {
            	this.filePathName.add(currentPathDirectory + "/" + f.getName());
            }
        }
    }
	
	/**
	 * Permet de compresser un fichier. Un fichier compressé est formaté de
	 * la manière suivante :
	 *  1 -> nom du premier fichier (son chemin à partir du dossier sélectionné)
	 *  2 -> ligne suivante : tableau d'occurence du fichier (pour récupérer l'arbre)
	 *  3 -> ligne suivante : nombre d'octet à lire
	 *  4 -> lignes suivantes : les données compressées
	 *  
	 * @param fileToCompress
	 * 		Fichier à compresser
	 * 
	 * @param fileCompress
	 * 		Le fichier compressé
	 * 
	 * @param path
	 * 		Chemin du fichier compressé
	 * 
	 * @throws IOException en cas de problème de lecture et/ou d'écriture
	 */
	private void compress_file(File fileToCompress, File fileCompress, String path) throws IOException {
		List<Integer> occurences = read(fileToCompress);
		create_code(create_tree(occurences));
		String codeChar = "";
		insert_data(fileToCompress, fileCompress, occurences, path);
		BufferedInputStream readFileToCompress = new BufferedInputStream(new FileInputStream(fileToCompress));
		OutputBitStream writeFileCompress = new OutputBitStream(fileCompress);
		// on lit le premier caractere du fichier � compresser
		// on recherche ce caractere dans notre liste code
		// on r�cup�re le code associ� � ce caract�re
		// on �crit bit � bit dans le fichier grace � la classe OutputBitStream
		for(int i = readFileToCompress.read(); i != -1; i = readFileToCompress.read()) {
			// pout chaque caractère lu dans le fichier à compresser
			// on récupère son code binaire associé grace à la méthode create_code
			codeChar = this.codes.get((char) i);
			// on écrit le code bit à bit dans le fichier compressé
			for(int j = 0; j < codeChar.length(); j++)
				writeFileCompress.write_bit(Character.getNumericValue(codeChar.charAt(j)));
		}
		readFileToCompress.close();
		writeFileCompress.close();
	}
	
	/**
	 * Insère le nom du fichier et son tableau d'occurence associé au début
	 * du fichier compressé
	 * 
	 * @param fileToCompress
	 * 		Fichier à compresser
	 * 
	 * @param fileCompress
	 * 		Fichier compressé
	 * 
	 * @param occurences
	 * 		Liste des occurences du fichier à compresser
	 * 
	 * @param path
	 * 		Chemin du fichier compressé
	 * 
	 * @throws IOException en cas de problème de lecture et/ou d'écriture
	 */
	private void insert_data(File fileToCompress, File fileCompress, List<Integer> occurences, String path) throws IOException {
		String occu = "";
		OutputStreamWriter writeDataFileCompress = new OutputStreamWriter(new FileOutputStream(fileCompress, true), "Cp1252");
		writeDataFileCompress.write(path + "\n");
		for(int el : occurences)
			occu += (el + SEPARATOR);
		writeDataFileCompress.write(occu + "\n");
		writeDataFileCompress.close();
	}
	
	/**
	 * Permet de décompresser un dossier.
	 * On commence par créer tous les dossiers existants
	 * en s'aidant du formatation de la compression
	 * @see Huffman#compress_directory(File, File)
	 * Il suffit alors récupérer le chemin de chaque fichier et
	 * de créer l'arbre de compression en récupurant le tableau d'occurence
	 * 
	 * @param readFileToDecompress
	 * 		Lecture du fichier à décompresser bit à bit ou ligne par ligne
	 * 
	 * @throws IOException en cas de problème de lecture et/ou d'écriture
	 */
	public void decompress_directory(InputBitStream readFileToDecompress) throws IOException {
		int nbDirectory = Integer.parseInt(readFileToDecompress.read_line());
		for(int i = 0; i < nbDirectory; i++)
			new File(readFileToDecompress.read_line()).mkdir();
		// on récupère le nombre de fichiers
		// et on les décompresse chacun leur tour
		int nbFiles = Integer.parseInt(readFileToDecompress.read_line());
		for(int i = 0; i < nbFiles; i++) {
			 decompress_file(readFileToDecompress, "");
			 // une fois qu'on a décompressé un fichier
			 // on passe à la ligne pour avoir accès aux données
			 // du fichier suivant
			 readFileToDecompress.read_line();
		}
	}
	
	/**
	 * Permet la décompression d'un fichier
	 * 
	 * @param readFileToDecompress
	 * 		Lecture bit à bit ou ligne par ligne du fichier à compresser
	 * 
	 * @throws IOException en cas de problème de lecture et/ou d'écriture
	 */
	private void decompress_file(InputBitStream readFileToDecompress, String path) throws IOException {
		File fileDecompress;
		if(!path.equals(""))
			fileDecompress = new File(path + "\\" + readFileToDecompress.read_line());
		else
			fileDecompress = new File(readFileToDecompress.read_line());
		fileDecompress.createNewFile();
		// on récupère la liste des occurences
		List<Integer> occurences = get_tab_occurence(readFileToDecompress);
		// on récupère le nombre de bits à lire
		long bits = Long.parseLong(readFileToDecompress.read_line());
		BufferedOutputStream writeFileDecompress = new BufferedOutputStream(new FileOutputStream(fileDecompress));
		// on recrée notre arbre à partir des donn�es r�cup�r�es
		Node root = create_tree(occurences);
		Node currentNode = root;
		// on lit le fichier compress� bit � bit via le InputBitSteam
		for(long i = 0; i < bits; i++) {
			// on va � gauche
			currentNode = readFileToDecompress.read_bit() == 0 ?  currentNode.get_left() : currentNode.get_right();
			// on v�rifie si le noeuf est une feuille
			/// si c'est le cas, on r�cup�re son symbol associ�
			// on l'�crit dans le fichier
			// et on revient � la racine de l'arbre
			if(currentNode.get_left() == null) {
				writeFileDecompress.write(currentNode.get_symbol());
				currentNode = root;
			}
		}
		readFileToDecompress.flush();
		writeFileDecompress.close();
	}
	
	/**
	 * Surcharge de la m�thode(File, String) pour permettre
	 * de d�compresser une liste de fichiers en faisant appel
	 * � la m�thode decompress_file(File, String)
	 * 
	 * @param filesToDecompress
	 * 		Liste de fichiers � d�compresser
	 * 
	 * @param path
	 * 		Si l'on souhaite pr�ciser le chemin absolu afin de cr�er
	 * 		les fichiers r�sultants dans le r�pertoire d�sir�
	 * 
	 * @return
	 * 		0 : d�compression effectu�e avec succ�s
	 * 		-1 : format d'un des fichiers � d�compresser incorrect
	 * 		-2 : probl�me de lecture dans un des fichiers � d�compresser ou
	 * 			 d'�criture dans un des fichiers r�sultants
	 */
	public int decompress(List<File> filesToDecompress, String path) {
		int result;
		for(File f : filesToDecompress)
			if((result = decompress(f, path)) != 0) return result;
		return 0;
	}
	/**
	 * Fonction de décompression, appel decompress_file
	 * ou decompress_directory selon la nature de la
	 * donnée sélectionnée
	 * 
	 * @param fileToDecompress
	 * 		La donnée à décompresser
	 * 
	 * @param path
	 * 		Si l'on souhaite pr�ciser le chemin absolu afin de cr�er
	 * 		les fichiers r�sultants dans le r�pertoire d�sir�
	 * 
	 * @return
	 * 		0 : d�compression effectu�e avec succ�s
	 * 		-1 : format du fichier � d�compresser incorrect
	 * 		-2 : probl�me de lecture dans le fichier � d�compresser ou
	 * 			 d'�criture dans le fichier r�sultant
	 */
	public int decompress(File fileToDecompress, String path) {
		// on lit les 3 premi�res lignes (nom du fichier, tableau d'occurence et tableau de symbole)
		// on recr�e l'arbre � partir de ces donn�es
		// on lit les donn�es du fichier compress�
		try {
			InputBitStream readFileToDecompress = new InputBitStream(new BufferedInputStream(new FileInputStream(fileToDecompress)));
			String type = readFileToDecompress.read_line();
			if(type.equals("f"))
				decompress_file(readFileToDecompress, path);	
			else if(type.equals("lf")) {
				int nbFiles = Integer.parseInt(readFileToDecompress.read_line());
				for(int i = 0; i < nbFiles; i++) {
					decompress_file(readFileToDecompress, path);
					readFileToDecompress.read_line();
				}
			}
			else if(type.equals("d"))
				decompress_directory(readFileToDecompress);
			else {
				readFileToDecompress.close();
				return -1;
			}
			readFileToDecompress.close();
		}
		catch(IOException e) {
			return -2;
		}
		return 0;
	}
	
	/**
	 * Récupère le tableau d'occurence dans le fichier sélectionné
	 * et transforme la donnée en un liste d'integer
	 * 
	 * @param readFileToDecompress
	 * 		Lecture bit à bit ou ligne par ligne du fichier à compresser
	 * 
	 * @return la liste d'occurence du fichier à décompresser
	 * 
	 * @throws IOException en cas de problème de lecture et/ou d'écriture
	 */
	private List<Integer> get_tab_occurence(InputBitStream readFileToDecompress) throws IOException {
		return  Arrays.asList(readFileToDecompress.read_line().split(SEPARATOR))
				.stream()
				.map(el->Integer.parseInt(el))
				.collect(Collectors.toList());
	}
	
	/**
	 * Récupère le taux de compression
	 * 
	 * @param fileCompress
	 * 		Fichier compressé
	 * @param fileToDecompress
	 * 		Fichier initial
	 * @return
	 */
	public double get_gain(File fileCompress, File fileToCompress) {
		return (((double) fileToCompress.length() - (double) fileCompress.length()) / (double) fileToCompress.length()) * 100;
	}
}
