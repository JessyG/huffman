package huffman;
/**
 * Classe représentant un noeud de l'arbre de compression
 * Elle implémente Comparable afin de pouvoir comparer facilement les noeuds
 * entre eux
 * 
 * @author Jessy
 *
 */
public class Node implements Comparable<Node> {
	/**
	 * L'occurence associé au noeud
	 * 
	 * @see Node#Node(int, Character)
	 * @see Node#get_occurence()
	 */
	private int occurence;
	
	/**
	 * Symbole associé au noeud, seules les feuilles
	 * en possèdent un.
	 * 
	 * @see Node#Node(int, Character)
	 * @see Node#get_symbol()
	 */
	private Character symbol;
	
	/**
	 * Fils gauche
	 */
	private Node left;
	
	/**
	 * Fils droit
	 */
	private Node right;
	
	public Node(int occu, Character sym) {
		this.occurence = occu;
		this.symbol = sym;
		this.left = null;
		this.right = null;
	}

	public int get_occurence() {
		return this.occurence;
	}
	
	public char get_symbol() {
		return this.symbol;
	}
	
	public Node get_left() {
		return this.left;
	}
	
	public void set_left(Node l) {
		this.left = l;
	}
	
	public Node get_right() {
		return this.right;
	}
	
	public void set_right(Node r) {
		this.right = r;
	}
	
	/**
	 * Les noeuds sont comparés entre-eux
	 * à partir de leur occurence
	 */
	@Override
	public int compareTo(Node n) {
		return this.occurence > n.occurence ? 1 : -1;
	}
}
