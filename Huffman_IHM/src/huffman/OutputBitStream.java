package huffman;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Classe qui permet d'écrire bit à bit dans un fichier de sortie
 * Etant donné qu'on ne peut écrire qu'au minimum un octet dans un fichier,
 * on doit passer par un buffer, qui une fois rempli (contenant 8 bits), sera transmis
 * dans le fichier de sortie
 * 
 * @author Jessy
 * 
 */

public class OutputBitStream {
	File fOut;
	
	/**
	 * Fichier intermédiaire dans lequel
	 * on écrira en premier lieu les données
	 * compressées
	 * 
	 * @see OutputBitStream#write_bit(int)
	 */
	File fcpy;
	
	/**
	 * Buffer qui servira à écrire
	 * dans le fichier fcpy
	 * 
	 * @see OutputBitStream#wOutputBitStream(File)
	 * @see OutputBitStream#write_bit(int)
	 */
	BufferedOutputStream copy;
	
	/**
	 * Buffer complété bit à bit et
	 * envoyé au fichier fcpy une fois
	 * rempli
	 */
	int buffer;
	
	/**
	 * Entier pour savoir à quel moment le
	 * buffer est rempli
	 */
	int length;
	
	/**
	 * Nombre de bit écrit dans le fichier
	 * 
	 * @see OutputBitStream#write_bit(int)
	 * @see OutputBitStream#wclose()
	 */
	long bits;
	
	/**
	 * Constructeur de la classe OutputBitStream
	 * on crée un fichier temporaire copy.txt pour
	 * écrire une première fois les données compressées
	 * à l'interieur
	 * 
	 * @param out
	 * 		Fichier compressé
	 */
	public OutputBitStream(File out) throws IOException {
		this.fOut = out;
		this.fcpy = new File("copy.txt");
		this.fcpy.createNewFile();
		this.copy = new BufferedOutputStream(new FileOutputStream(this.fcpy));
		this.length = 0;
		this.bits = 0;
	}
	
	/**
	 * Permet d'écrire bit à bit dans le fichier fcpy
	 * 
	 * @param bit
	 * 		bit à écrire
	 * 
	 * @throws IOException en cas d'un problème de lecture du fichier
	 */
	public void write_bit(int bit) throws IOException {
		// si le bit est égal à 0, il suffit de décaler les bits du buffer d'un rang vers la gauche
		if(bit==0)
			this.buffer <<= 1;
		// si le bit est égal à 1
		// on réalise un décalage d'un rang vers la gauche
		// puis on applique l'opérateur ou logique
		// afin de transformer le dernier bit en un 1
		else
			this.buffer = this.buffer << 1 | 1;
		length++;
		// lorsque le buffer est complet
		// on l'écrit dans le fichier fcpy
		// puis on réinitialise les variables
		// on pense à incrémenter le nombre de bits écrit
		// dans le fichier
		if (length == 8) {
		    this.copy.write(buffer);
		    this.bits++;
		    buffer = 0;
		    length = 0;
		}
	}
	
	/**
	 * Permet de copier le fichier fcpy (dans
	 * lequel se trouve les données compressées)
	 * dans le fichier compressé en y ajoutant au
	 * début du fichier le nombre de bits à lire qui
	 * sera une donnée utile en cas de décompression
	 * 
	 * @see Huffman#decompress_file(InputBitStream)
	 * 
	 * @throws IOException en cas de problème de lecture et/ou d'écriture
	 */
	public void copy() throws IOException {
		char newLine = '\n';
		BufferedInputStream brr = new BufferedInputStream(new FileInputStream(this.fcpy));
		byte[] buf = new byte[1024];
		int len = 0;
		OutputStreamWriter outFirst = new OutputStreamWriter(new FileOutputStream(this.fOut, true), "Cp1252");
		outFirst.write(new Long(this.bits).toString());
		outFirst.write("\n");
		outFirst.close();
		BufferedOutputStream outNext = new BufferedOutputStream(new FileOutputStream(this.fOut, true));
		//OutputStream outNext = new FileOutputStream(this.fOut, true);
		while ((len = brr.read(buf)) > 0)
            outNext.write(buf, 0, len);
		outNext.write((byte) newLine);
		brr.close();
		outNext.close();
	}
	
	/**
	 * Fonction pour fermer le flux et insérer le dernier octet dans le fichier
	 * On ne souhaite pas lire les bits ajoutés pour remplir le buffer, ainsi
	 * on soustrait ces derniers pour connaitre le nombre de bits précis que l'on
	 * devra lire en cas de décompression
	 * 
	 * @see Huffman#decompress_file(InputBitStream)
	 */
	public void close() throws IOException {
		this.copy.write(this.buffer << (8 - length));
		this.bits = (this.bits + 1) * 8 - (8 - length);
		this.copy.close();
		copy();
		fcpy.delete();
	}
}
