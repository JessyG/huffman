package huffman;
import java.io.BufferedInputStream;
import java.io.IOException;

/**
 * Classe permettant de lire un fichier bit à bit
 * ou ligne par ligne
 * 
 * @author Jessy
 *
 */
public class InputBitStream {
	/**
	 * Donnée qui sera lue bit à bit ou ligne par ligne
	 * 
	 * @see InputBitStream#InputBitStream(BufferedInputStream)
	 * @see  InputBitStream#read_line()
	 * @see  InputBitStream#read_bit()
	 */
	BufferedInputStream in;
	
	/**
	 * Entier permettant de parcourir un octet bit à bit
	 * 
	 * @see  InputBitStream#read_bit()
	 */
	int mask;
	
	/**
	 * buffer qui sera lu bit à bit
	 * grace au masque
	 * 
	 * @see  InputBitStream#read_bit()
	 */
	int buffer;
	
	public InputBitStream(BufferedInputStream in) {
		this.in = in;
	}
	
	public String read_line() throws IOException {
		String result = "";
		int c;
		while((c = this.in.read()) != -1) {
			if((char) c == '\n') return result;
			result += (char) c;
		}
		return result;
	}
	
	public int read_bit() throws IOException {
	    int buffer;
	    int mask = this.mask;
	    // si le masque est �gal � 0
	    // c'est qu'on a lu un octet complet
	    // on lit donc l'octet suivant dans le fichier d'entr�e
	    if(mask == 0) {
	    	buffer = this.in.read();
	    	if(buffer == -1)
	    		return -1;
	    	this.buffer = buffer;
	    	mask = 128;
	    }
	    else {
	      buffer = this.buffer;
	    }
	    // 128 en binaire = 10000000
	    // en appliquant 8 d�placements vers la gauche
	    // on aura lu un octet complet et mask sera �gal � 0
	    this.mask = mask >> 1;
	    // en appliquant l'op�ratuer et logique
	    // et en d�calant les bits de la variable mask d'un rang vers la gauche � chaque appel 
	    // on lire le buffer bit � bit
	    if((buffer & mask) == 0)
	      return 0;
	    else
	      return 1;
	}
	
	/**
	 * Permet de réinitialiser le mask.
	 * Utile si l'on souhaite décompresser plusieurs
	 * fichiers en même temps
	 * 
	 * @see Huffman#decompress_file(InputBitStream)
	 */
	public void flush() {
		this.mask = 0;
	}
	
	public void close() throws IOException {
		this.in.close();
	}
}
